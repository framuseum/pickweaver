defmodule Pickweaver.Repo.Migrations.CreateTags do
  use Ecto.Migration

  def change do
    create table(:tags) do
      add :slug, :string
      add :name, :string

      timestamps(updated_at: false)
    end

  end
end
