defmodule Pickweaver.Repo.Migrations.AddElementsTable do
  use Ecto.Migration

  def change do
    create table(:elements, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :url, :string
      add :data, :map

      timestamps()
    end

    alter table(:stories) do
      remove :elements
      add :elements, :map
    end
  end
end
