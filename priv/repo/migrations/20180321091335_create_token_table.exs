defmodule Pickweaver.Repo.Migrations.CreateTokenTable do
  use Ecto.Migration

  def change do
    create table(:token) do
      add :service, :string
      add :data, :map
      add :account_id, references(:accounts)

      timestamps()
    end
  end
end
