defmodule :"Elixir.Pickweaver.Repo.Migrations.Add-user-password-reset-fields" do
  use Ecto.Migration

  def up do
    alter table(:accounts) do
      add :reset_password_sent_at, :utc_datetime
      add :reset_password_token, :string
    end
    create unique_index("accounts", [:reset_password_token], name: "index_unique_accounts_reset_password_token")
  end

  def down do
    alter table(:accounts) do
      remove :reset_password_sent_at
      remove :reset_password_token
    end
  end
end
