defmodule Pickweaver.Repo.Migrations.AddTypeToElements do
  use Ecto.Migration

  def up do
    TypeEnum.create_type
    alter table(:elements) do
      add :type, :type
    end
  end

  def down do
    alter table(:elements) do
      remove :type
    end
    TypeEnum.drop_type
  end
end
