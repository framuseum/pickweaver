defmodule Pickweaver.Repo.Migrations.ChangePublicationSystem do
  use Ecto.Migration

  def up do
    alter table("stories") do
      add :published, :bool, [null: false, default: false]
      remove :published_at
    end
  end

  def down do
    alter table("stories") do
      add :published_at, :utc_datetime, null: true
      remove :published
    end
  end
end
