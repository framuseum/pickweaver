defmodule Pickweaver.Repo.Migrations.IncreaseUrlLength do
  use Ecto.Migration

  def up do
    alter table("elements") do
      modify :url, :string, size: 2048
    end
  end

  def down do
    alter table("elements") do
      modify :url, :string, size: 256
    end
  end
end
