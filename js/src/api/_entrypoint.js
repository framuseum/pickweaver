/* eslint-disable */

export const API_HOST = process.env.API_HOST || 'localhost:4000';
export const API_ORIGIN = process.env.API_ORIGIN || 'http://localhost:4000';
export const API_WEBSOCKET = process.env.API_WEBSOCKET || 'wss://localhost:4000';
export const API_PATH = process.env.API_PATH || '/api/v1';

/* eslint-enable */
