const ImportStorifyProfile = () => import(/* webpackChunkName: "import-storify-profile" */'@/components/import/storify/ImportProfile');
const ImportStorifyURL = () => import(/* webpackChunkName: "import-storify-url" */'@/components/import/storify/ImportURL');
const ImportStorify = () => import(/* webpackChunkName: "import-storify" */'@/components/import/storify/ImportStorify');
const ImportTwitter = () => import(/* webpackChunkName: "import-twitter" */'@/components/import/twitter/ImportTwitter');
const ImportTwitterMoment = () => import(/* webpackChunkName: "import-twitter-moments" */'@/components/import/twitter/ImportTwitterMoment');
const ImportTwitterTweets = () => import(/* webpackChunkName: "import-twitter-tweets" */'@/components/import/twitter/ImportTwitterTweets');
const ImportQueue = () => import(/* webpackChunkName: "import-queue" */'@/components/import/Queue');

export default [
  {
    path: '/import/storify',
    name: 'ImportStorify',
    component: ImportStorify,
    meta: { requiresAuth: true },
  },
  {
    path: '/import/storify/url',
    name: 'ImportStorifyURL',
    component: ImportStorifyURL,
    meta: { requiresAuth: true },
  },
  {
    path: '/import/storify/profile',
    name: 'ImportStorifyProfile',
    component: ImportStorifyProfile,
    meta: { requiresAuth: true },
  },
  {
    path: '/import/twitter',
    name: 'ImportTwitter',
    component: ImportTwitter,
    meta: { requiresAuth: true },
  },
  {
    path: '/import/twitter_moment',
    name: 'ImportTwitterMoment',
    component: ImportTwitterMoment,
    meta: { requiresAuth: true },
  },
  {
    path: '/import/twitter_tweets',
    name: 'ImportTwitterTweets',
    component: ImportTwitterTweets,
    meta: { requiresAuth: true },
  },
  {
    path: '/import/queue',
    name: 'ImportQueue',
    component: ImportQueue,
    meta: { requiresAuth: true },
  },
];
