defmodule PickweaverWeb.ImportView do
  use PickweaverWeb, :view
  require Logger
  alias PickweaverWeb.ImportView
  alias PickweaverWeb.AccountView

  def render("imports.json", %{imports: imports}) do
    render_many(imports, ImportView, "import.json")
  end

  def render("import.json", %{import: import}) do
    %{
      id: import.id,
      created_at: import.inserted_at,
      updated_at: import.updated_at,
      account: render_one(import.account, AccountView, "show_private.json"),
      tentatives: import.tentatives,
      status: import.status,
      url: import.url,
    }
  end
end
