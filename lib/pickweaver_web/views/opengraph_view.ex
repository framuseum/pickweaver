defmodule PickweaverWeb.OpenGraphView do
  require Logger
  use PickweaverWeb, :view
  alias PickweaverWeb.ViewHelpers

  def render("opengraph.json", opengraph) do
    opengraph = ViewHelpers.keys_to_atoms(opengraph.opengraph)
    %{
      type: "opengraph",
      title: opengraph.title,
      description: opengraph.description,
      url: opengraph.url,
      id: opengraph.url,
      domain: urlToDomain(opengraph.url),
      image: %{url: opengraph.image},
    }
  end

  def toLinks(text) do
    if text != nil do
      urls = Regex.scan(~r/(http|ftp|https):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])?/iu, text, [capture: :first])
      urls
      |> processURLs()
      |> replaceURLs(text)
    else
      nil
    end
  end

  defp processURLs(urls) do
    Enum.map(urls, fn url -> %{url: url, html: processURL(url)} end)
  end

  def processURL([url]) do
    "<a class=\"card-link\" href=\"" <> url <> "\">" <> url <> "</a>"
  end

  defp replaceURLs([head | tail], body) do
    body = replaceURLs(tail, body)
    body = replaceURL(head, body)
    body
  end

  defp replaceURLs([], body) do
    body
  end

  defp replaceURL(url_map, body) do
    String.replace(body, hd(url_map.url), url_map.html)
  end

  def urlToDomain(url) do
    fuzzyurl = Fuzzyurl.from_string(url)
    fuzzyurl.hostname
    |> stripwww
  end

  def stripwww("www." <> domain) do domain end

  def stripwww(domain) do domain end
end
