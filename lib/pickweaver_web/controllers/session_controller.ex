defmodule PickweaverWeb.SessionController do
  use PickweaverWeb, :controller
  use PhoenixSwagger

  alias Pickweaver.Accounts.Account
  alias Pickweaver.Accounts

  swagger_path :sign_in do
    summary "Sign-in"
    description "Sign-in with username and password"
    parameters do
      username :body, :string, "The username of the account", required: true
      password :body, :string, "The password of account", required: true
    end
    response 200, "Ok", (Schema.new do
      properties do
        token :string, "A JWT token", example: "eyJhbGciOiJIUz[...]"
        user Schema.ref(:Account), "The logged-in account"
      end
    end)
    response 401, "Unauthorized", Schema.ref(:AuthError)
  end
  def sign_in(conn, %{"username" => username, "password" => password}) do
    with {:ok, %Account{} = account} <- Accounts.find_by_username(username),
         {:ok, %Account{} = _account} <- is_confirmed(account),
         {:ok, token, _claims} <- Accounts.authenticate(%{account: account, password: password}) do
            # Render the token
            render conn, "token.json", %{token: token, user: account}
    else
      {:error, :not_found} ->
        conn
        |> put_status(401)
        |> json(%{"error_msg" => "No such user", "display_error" => "session.error.bad_login"})
      {:error, :unconfirmed} ->
        conn
        |> put_status(401)
        |> json(%{"error_msg" => "User is not activated", "display_error" => "session.error.not_activated"})
      {:error, :unauthorized} ->
        conn
        |> put_status(401)
        |> json(%{"error_msg" => "Bad login", "display_error" => "session.error.bad_login"})
    end
  end

  defp is_confirmed(%Account{confirmed_at: confirmed_at} = account) do
    if is_nil confirmed_at do
      {:error, :unconfirmed}
    else
      {:ok, account}
    end
  end

  swagger_path :sign_out do
    summary "Sign-out"
    description "Sign-out"
    response 204, "No Content"
  end
  def sign_out(conn, _params) do
    conn
    |> PickweaverWeb.Guardian.Plug.sign_out()
    |> send_resp(204, "")
  end

  def show(conn, _params) do
    user = PickweaverWeb.Guardian.Plug.current_resource(conn)

    send_resp(conn, 200, Poison.encode!(%{user: user}))
  end

  def swagger_definitions do
  %{
    AuthError: swagger_schema do
      properties do
        error_msg :string, "A string identifier for the error"
        display_error :string, "A translation key to display for the error"
      end
      example %{
        "error_msg" => "Bad login",
        "display_error" => "session.error.bad_login"
      }
    end
  }
  end
end
