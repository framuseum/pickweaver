defmodule Pickweaver.Services.Parser.Image do
  @moduledoc """
  Just fetch an inline image
  """

  defstruct [:src, :type, :size]
end
