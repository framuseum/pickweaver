defmodule Pickweaver.Services.Parser.ParserError do
  defexception message: "Parser Error"
end
