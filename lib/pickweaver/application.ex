defmodule Pickweaver.Application do
  @moduledoc """
  Pickweaver's app
  """
  use Application

  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec

    # Define workers and child supervisors to be supervised
    children = [
      # Start the Ecto repository
      supervisor(Pickweaver.Repo, []),
      # Start the endpoint when the application starts
      supervisor(PickweaverWeb.Endpoint, []),
      # Start your own worker by calling: Pickweaver.Worker.start_link(arg1, arg2, arg3)
      # worker(Pickweaver.Worker, [arg1, arg2, arg3]),
      worker(Guardian.DB.Token.SweeperServer, []),
      worker(Cachex, [:my_cache, []])
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Pickweaver.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    PickweaverWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
