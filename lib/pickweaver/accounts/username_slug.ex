defmodule Pickweaver.Accounts.UsernameSlug do
  @moduledoc """
  System to generate a slug for an account
  """
  alias Pickweaver.Accounts.Account
  use EctoAutoslugField.Slug, from: :username, to: :slug
  import Ecto.Query
  alias Pickweaver.Repo

  def build_slug(sources, changeset) do
    slug = super(sources, changeset)
    build_unique_slug(slug, changeset)
  end

  defp build_unique_slug(slug, changeset) do
    query = from a in Account,
      where: a.slug == ^slug

    case Repo.one(query) do
      nil -> slug
      _account ->
        slug
        |> Pickweaver.Slug.increment_slug()
        |> build_unique_slug(changeset)
    end
  end

  defp increment_slug(slug) do
    case List.pop_at(String.split(slug, "-"), -1) do
      {suffix, slug_parts} ->
        case Integer.parse(suffix) do
          {id, _} -> Enum.join(slug_parts, "-") <> "-" <> Integer.to_string(id + 1)
          :error -> slug <> "-1"
        end
      {nil, slug_parts} ->
        slug
    end
  end
end
