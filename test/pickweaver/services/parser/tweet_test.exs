defmodule Pickweaver.Services.TweetTest do
  @moduledoc """
  Test the `Pickweaver.Services.Import.TwitterTweets` module
  """

  use Pickweaver.DataCase
  alias Pickweaver.Services.Parser.Tweet
  use ExVCR.Mock, adapter: ExVCR.Adapter.Httpc
  import Pickweaver.Factory

  setup_all do
    ExVCR.Config.filter_url_params(true)
    ExVCR.Config.filter_sensitive_data("oauth_signature=[^\"]+", "<REMOVED>")
    ExVCR.Config.filter_sensitive_data("guest_id=.+;", "<REMOVED>")
    ExVCR.Config.filter_sensitive_data("access_token\":\".+?\"", "access_token\":\"<REMOVED>\"")
    ExVCR.Config.cassette_library_dir("test/fixtures/vcr_cassettes")
    :ok
  end

  describe "processing tweets" do

    @tweet_one %{
      text: "Want to freak yourself out? I'm gonna show just how much of your information the likes of Facebook and Google store about you without you even realising it",
      id: "977559925680467968"
    }
    @tweet_two %{
      text: "Et hop, nous voici partis pour un nouveau thread ! Et cette fois-ci, nous allons parler de l'évolution de l'identité visuelle de France Télévisions. Vous êtes prêts ? Déroulez donc 😉\n\n#habillage #francetelevisions ",
      id: "957685409160597507"
    }

    test "processing a tweet from a ExTwitter tweet ID" do
      use_cassette "twitt-977559925680467968" do
        [tweet] = ExTwitter.lookup_status("977559925680467968", [tweet_mode: "extended"])
        tweet_struct = struct(Tweet, @tweet_one)
        processed_tweet = Tweet.process_tweet(tweet)
        assert tweet_struct.id   == processed_tweet.id
        assert tweet_struct.text == processed_tweet.text
      end
    end

    test "transform a list of URLs info tweets elements" do
      use_cassette "tweet_structs" do
        account = insert(:account)
        tweet_structs = [struct(Tweet, @tweet_one), struct(Tweet, @tweet_two)]
        parsed_twitts = Tweet.parse_twitter_url_list(
                 [
                   "https://twitter.com/tedromeda/status/957556639221022720",
                   "https://twitter.com/valentinsocha/status/957685409160597507"
                 ],
                   account
        )
        assert length(parsed_twitts) == 2
        [tweet_one, tweet_two] = tweet_structs
        assert Enum.any?(tweet_structs, fn t -> tweet_one.id == t.id and tweet_one.text == t.text end)
        assert Enum.any?(tweet_structs, fn t -> tweet_two.id == t.id and tweet_two.text == t.text end)
      end
    end
  end
end
